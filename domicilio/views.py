from django.shortcuts import render, redirect
from .models import Zona,Locacion,Anexo
from .forms import ZonaForm, LocacionForm, AnexoForm

def cargar_locacion(request):
    zona_id = request.GET.get('zona')
    print('....................')
    print(zona_id)
    locacion = Locacion.objects.filter(zona_id=zona_id).order_by('nombre')
    context = {
        'locacion': locacion
    }
    return render(request, 'administration/locacion_dropdown_list_options.html', context)

def cargar_anexo(request):
    locacion_id = request.GET.get('locacion')
    anexo = Anexo.objects.filter(locacion_id=locacion_id).order_by('nombre')
    context = {
        'anexo': anexo
    }
    return render(request, 'others/anexo_dropdown_list_options.html', context)


def add_zona(request):
    forms = ZonaForm()
    if request.method == 'POST':
        forms = ZonaForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('zona')
    zona = Zona.objects.all()
    context = {'forms': forms, 'zona': zona}
    return render(request, 'domicilio/zona.html', context)

def add_locacion(request):
    forms = LocacionForm()
    if request.method == 'POST':
        forms = LocacionForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('locacion')
    locacion = Locacion.objects.all()
    context = {'forms': forms, 'locacion': locacion}
    return render(request, 'domicilio/locacion.html', context)

def add_anexo(request):
    forms = AnexoForm()
    if request.method == 'POST':
        forms = AnexoForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('anexo')
    anexo = Anexo.objects.all()
    context = {'forms': forms, 'anexo': anexo}
    return render(request, 'domicilio/anexo.html', context)

