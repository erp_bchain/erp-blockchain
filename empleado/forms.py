from django import forms
from . import models
from procesos.models import Departmento
class PersonalInfoForm(forms.ModelForm):
    class Meta:
        model = models.PersonalInfo
        exclude = {'direccion', 'educacion', 'capacitacion', 'trabajo', 'experiencia', }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'foto': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'fecha_nac': forms.DateInput(attrs={'class': 'form-control'}),
            'lugar_nac': forms.TextInput(attrs={'class': 'form-control'}),
            'nacionalidad': forms.Select(attrs={'class': 'form-control'}),
            'religion': forms.Select(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'grupo_sanguineo': forms.Select(attrs={'class': 'form-control'}),
            'e_tin': forms.NumberInput(attrs={'class': 'form-control'}),
            'nid': forms.NumberInput(attrs={'class': 'form-control'}),
            'licencia_manejo_pasaporte': forms.NumberInput(attrs={'class': 'form-control'}),
            'numero_teleono': forms.NumberInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'nombre_padre': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_madre': forms.TextInput(attrs={'class': 'form-control'}),
            'estado_marital': forms.Select(attrs={'class': 'form-control'}),

        }

class DireccionInfoForm(forms.ModelForm):
    class Meta:
        model = models.EmpleadoDireccionInfo
        fields = ('zona', 'locacion', 'anexo', 'cp')
        widgets = {
            'zona': forms.Select(attrs={'class': 'form-control'}),
            'locacion': forms.Select(attrs={'class': 'form-control'}),
            'anexo': forms.Select(attrs={'class': 'form-control'}),
            'cp': forms.TextInput(attrs={'class': 'form-control'})
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['locacion'].queryset = models.Locacion.objects.none()

            if 'locacion' in self.data:
                try:
                    zona_id = int(self.data.get('zona'))
                    self.fields['locacion'].queryset = models.Locacion.objects.filter(zona_id=zona_id).order_by('nombre')
                except (ValueError, TypeError):
                    pass
            elif self.instance.pk:
                self.fields['locacion'].queryset = self.instance.zona.locacion.order_by('nombre')

            self.fields['anexo'].queryset = models.Anexo.objects.none()

            if 'anexo' in self.data:
                try:
                    locacion_id = int(self.data.get('locacion'))
                    self.fields['anexo'].queryset = models.Anexo.objects.filter(locacion_id=locacion_id).order_by('nombre')
                except (ValueError, TypeError):
                    pass
            elif self.instance.pk:
                self.fields['anexo'].queryset = self.instance.locacion.anexo_set.order_by('nombre')



class EducacionInfoForm(forms.ModelForm):
    class Meta:
        model = models.EducacionInfo
        fields = '__all__'
        widgets = {
            'nombre_de_evaluacion': forms.TextInput(attrs={'class': 'form-control'}),
            'instituto': forms.TextInput(attrs={'class': 'form-control'}),
            'grupo': forms.TextInput(attrs={'class': 'form-control'}),
            'grado': forms.TextInput(attrs={'class': 'form-control'}),
            'reunion': forms.TextInput(attrs={'class': 'form-control'}),
            'anio_presente': forms.NumberInput(attrs={'class': 'form-control'}),
        }

class EntrenamientoInfoForm(forms.ModelForm):
    class Meta:
        model = models.EntrenamientoInfo
        fields = '__all__'
        widgets = {
            'nombre_entrenamiento': forms.TextInput(attrs={'class': 'form-control'}),
            'anio': forms.NumberInput(attrs={'class': 'form-control'}),
            'duracion': forms.NumberInput(attrs={'class': 'form-control'}),
            'lugar': forms.TextInput(attrs={'class': 'form-control'}),
        }

class EmpleadoTrabajoInfoForm(forms.ModelForm):
    class Meta:
        model = models.EmpleadoTrabajoInfo
        fields = '__all__'
        widgets = {
            'categoria': forms.Select(attrs={'class': 'form-control'}),
            'dia_de_ingreso': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_instituto': forms.TextInput(attrs={'class': 'form-control'}),
            'designacion_de_trabajo': forms.Select(attrs={'class': 'form-control'}),
            'departmento': forms.Select(attrs={'class': 'form-control'}),
            'escala': forms.NumberInput(attrs={'class': 'form-control'}),
            'grado_publicacion': forms.TextInput(attrs={'class': 'form-control'}),
            'primer_periodo_de_contrato': forms.NumberInput(attrs={'class': 'form-control'}),
            'segundo_periodo_de_contrato': forms.NumberInput(attrs={'class': 'form-control'}),
            'promocion_de_vencimiento': forms.NumberInput(attrs={'class': 'form-control'}),
            'renovacion_contrato': forms.NumberInput(attrs={'class': 'form-control'}),
            'tiempo_jubilacion': forms.NumberInput(attrs={'class': 'form-control'}),
        }

class ExperienciaInfoForm(forms.ModelForm):
    class Meta:
        model = models.ExperienciaInfo
        fields = '__all__'
        widgets = {
            'nombre_instituto': forms.TextInput(attrs={'class': 'form-control'}),
            'designation': forms.TextInput(attrs={'class': 'form-control'}),
            'entrenador': forms.TextInput(attrs={'class': 'form-control'}),
        }
