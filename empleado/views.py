from django.shortcuts import render,redirect
from . import forms
from .models import Zona,Locacion,Anexo,PersonalInfo
# Create your views here.
def cargar_locacion(request):
    zona_id = request.GET.get('zona')
    locacion = Locacion.objects.filter(zona_id=zona_id).order_by('nombre')

    locacion_id = request.GET.get('locacion')
    anexo = Anexo.objects.filter(locacion_id=locacion_id).order_by('nombre')
    context = {
        'locacion': locacion,
        'anexo': anexo
    }
    return render(request, 'others/locacion_dropdown_list_options.html', context)


def registro_gerente(request):
    form = forms.PersonalInfoForm()
    formulario_direccion = forms.DireccionInfoForm()
    formulario_educacion = forms.EducacionInfoForm()
    formulario_entreamiento = forms.EntrenamientoInfoForm()
    formulario_de_trabajo = forms.EmpleadoTrabajoInfoForm()
    formulario_de_experiencia = forms.ExperienciaInfoForm()
    if request.method == 'POST':
        form = forms.PersonalInfoForm(request.POST, request.FILES)
        formulario_direccion = forms.DireccionInfoForm(request.POST)
        formulario_educacion = forms.EducacionInfoForm(request.POST)
        formulario_entreamiento = forms.EntrenamientoInfoForm(request.POST)
        formulario_de_trabajo = forms.EmpleadoTrabajoInfoForm(request.POST)
        formulario_de_experiencia = forms.ExperienciaInfoForm(request.POST)
        if form.is_valid() and formulario_direccion.is_valid() and formulario_educacion.is_valid() and formulario_entreamiento.is_valid() and formulario_de_trabajo.is_valid() and formulario_de_experiencia.is_valid():
            personal_info = form.save()
            direccion_info = formulario_direccion.save(commit=False)
            direccion_info.direccion = personal_info
            direccion_info.save()
            educacion_info = formulario_educacion.save(commit=False)
            educacion_info.educacion = personal_info
            educacion_info.save()
            entrenamiento_info = formulario_entreamiento.save(commit=False)
            entrenamiento_info.entrenamiento = personal_info
            entrenamiento_info.save()
            trabajo_info = formulario_de_trabajo.save(commit=False)
            trabajo_info.trabajo = personal_info
            trabajo_info.save()
            experiencia_info = formulario_de_experiencia.save(commit=False)
            experiencia_info.experiencia = personal_info
            experiencia_info.save()
            return redirect('empleado-list')

    context = {
        'form': form,
        'formulario_direccion': formulario_direccion,
        'formulario_educacion': formulario_educacion,
        'formulario_entreamiento': formulario_entreamiento,
        'formulario_de_trabajo': formulario_de_trabajo,
        'formulario_de_experiencia': formulario_de_experiencia


    }
    return render(request, 'empleado/empleado-registration.html', context)


def lista_gerente(request):
    gerente = PersonalInfo.objects.all()
    context = {'gerente': gerente}
    return render(request, 'empleado/empleado-list.html', context)

