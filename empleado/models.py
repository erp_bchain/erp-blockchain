from django.db import models
from domicilio.models import Zona,Locacion,Anexo
from administracion.models import Designacion
from procesos.models import Departmento

# Create your models here.
class EmpleadoDireccionInfo(models.Model):
    zona = models.ForeignKey(Zona, on_delete=models.CASCADE, null=True)
    locacion = models.ForeignKey(Locacion, on_delete=models.CASCADE, null=True)
    anexo = models.ForeignKey(Anexo, on_delete=models.CASCADE, null=True)
    cp = models.TextField()

    def __str__(self):
        return self.cp

class EducacionInfo(models.Model):
    nombre_de_evaluacion = models.CharField(max_length=100)
    instituto = models.CharField(max_length=255)
    grupo = models.CharField(max_length=100)
    grado = models.CharField(max_length=45)
    reunion = models.CharField(max_length=45)
    anio_presente = models.IntegerField()

    def __str__(self):
        return self.nombre_de_evaluacion

class EntrenamientoInfo(models.Model):
    nombre_entrenamiento = models.CharField(max_length=100)
    anio = models.IntegerField()
    duracion = models.IntegerField()
    lugar = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre_entrenamiento

class EmpleadoTrabajoInfo(models.Model):
    eleccion_de_categoria = (
        ('bcs', 'BCS'),
        ('nacionalizado', 'nacionalizado'),
        ('cuota del 10%', 'cuota del 10%'),
        ('no gubernamental', 'no gubernamental')
    )
    categoria = models.CharField('Categoría',choices=eleccion_de_categoria, max_length=45)
    dia_de_ingreso = models.DateField('Día de ingreso')
    nombre_instituto = models.CharField('Nombre de Instituto',max_length=100)
    designacion_de_trabajo = models.ForeignKey(Designacion, on_delete=models.CASCADE)
    departmento = models.ForeignKey(Departmento, on_delete=models.CASCADE)
    escala = models.IntegerField('Escala')
    grado_publicacion = models.CharField('Grado de publicación',max_length=45)
    primer_periodo_de_contrato = models.IntegerField('Primer periodo de tiempo vencimiento de contrato')
    segundo_periodo_de_contrato = models.IntegerField('Segundo periodo de tiempo vencimiento de contrato')
    promocion_de_vencimiento = models.IntegerField('Promocion de vencimiento año/contrato')
    renovacion_contrato = models.IntegerField('Licencia de renovación por año/contrato')
    tiempo_jubilacion = models.IntegerField('Año jubilación esperado')


    def __str__(self):
        return self.nombre_instituto

class ExperienciaInfo(models.Model):
    nombre_instituto = models.CharField('Nombre del Instituto *',max_length=100)
    designation = models.CharField('Designación',max_length=45)
    entrenador = models.CharField('Capacitador',max_length=45)


    def __str__(self):
        return self.nombre_instituto

class PersonalInfo(models.Model):
    nombre = models.CharField('Nombre',max_length=45)
    foto = models.ImageField('Foto')
    fecha_nac = models.DateField('Fecha de nacimiento')
    lugar_nac = models.CharField('Lugar de nacimiento',max_length=45)
    eleccion_nacionalidad = (
        ('México', 'México'),
        ('Alemania', 'Alemania'),
        ('Rusia', 'Rusia'),
        ('Usa', 'Usa'),
        ('Otros', 'Otros')
    )
    nacionalidad = models.CharField('Nacionalidad',max_length=45, choices=eleccion_nacionalidad)
    eleccion_religion = (
        ('Islam', 'Islam'),
        ('Hinduismo', 'Hinduismo'),
        ('budismo', 'budismo'),
        ('cristianismo', 'cristianismo'),
        ('Otros', 'Otros')
    )
    religion = models.CharField('Religión',max_length=45, choices=eleccion_religion)
    eleccion_de_genero = (
        ('masculino', 'Masculino'),
        ('femenino', 'Femenino'),
        ('otro', 'Otro')
    )
    genero = models.CharField('Género',choices=eleccion_de_genero, max_length=10)
    eleccion_del_grupo_sanguineo = (
        ('a+', 'A+'),
        ('o+', 'O+'),
        ('b+', 'B+'),
        ('ab+', 'AB+'),
        ('a-', 'A-'),
        ('o-', 'O-'),
        ('b-', 'B-'),
        ('ab-', 'AB-')
    )
    grupo_sanguineo = models.CharField('Grupo sanguíneo',choices=eleccion_del_grupo_sanguineo, max_length=5)
    e_tin = models.IntegerField(unique=True)
    nid = models.IntegerField(unique=True)
    licencia_manejo_pasaporte = models.IntegerField('licencia de conducir/pasaporte',unique=True)
    numero_teleono = models.CharField('Número de telefono',max_length=11, unique=True)
    email = models.CharField(max_length=255, unique=True)
    nombre_padre = models.CharField('Nombre del Padre',max_length=45)
    nombre_madre = models.CharField('Nombre de la Madre',max_length=45)
    eleccion_de_estado_marital = (
        ('casado', 'Casado'),
        ('viudo', 'viudo'),
        ('separados', 'Separados'),
        ('divorciado', 'Divorciado'),
    )
    estado_marital = models.CharField('Estado Marital',choices=eleccion_de_estado_marital, max_length=10)
    direccion = models.ForeignKey(EmpleadoDireccionInfo, on_delete=models.CASCADE, null=True)
    educacion = models.ForeignKey(EducacionInfo, on_delete=models.CASCADE, null=True)
    capacitacion = models.ForeignKey(EntrenamientoInfo, on_delete=models.CASCADE, null=True)
    trabajo = models.ForeignKey(EmpleadoTrabajoInfo, on_delete=models.CASCADE, null=True)
    experiencia = models.ForeignKey(ExperienciaInfo, on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return self.nombre
