App={
    contracts :{},
    init: async () => {
        console.log('Cargando')
        await App.loadEthereum()
        await App.loadAccount()
        await App.loadContracts()
        App.render()
        await App.renderTasks()
    },
    loadEthereum:async()=>{
         
      if (window.ethereum) {
        App.web3Provider = window.ethereum;
        await window.ethereum.request({ method: "eth_requestAccounts" });
      } else if (web3) {
        web3 = new Web3(window.web3.currentProvider);
      } 
      else 
      {
      console.log("No ethereum browser is installed. Try it installing MetaMask ");
      }
    },
    loadAccount:async ()=>{
      const accounts= await window.ethereum.request({ method: "eth_requestAccounts" })
      App.account=accounts[0]
    },
    loadContracts: async () =>{
      
      const res = await fetch("TasksContract.json");
      const tasksContractJSON = await res.json()

      App.contracts.TasksContract = TruffleContract(tasksContractJSON)
      App.contracts.TasksContract.setProvider(App.web3Provider)
      App.tasksContract = await App.contracts.TasksContract.deployed()
    },
    render:()=>{
      document.getElementById('account').innerText = App.account

    },
    renderTasks: async () => {
      const tasksCounter = await App.tasksContract.taskCounter()
      const taskCounterNumber = tasksCounter.toNumber()
    },
    createTask:async(title,description)=>{
      const result =await App.tasksContract.createTask(title,description,{
      from: App.account
      })
      console.log(result.logs[0].args)
    }
  
}
