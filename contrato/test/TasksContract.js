const TaskContract= artifacts.require("TaskContract")

contract("TasksContract",()=>{
    before(async()=>{
        tasksContract =await tasksContract.deployed()
    })
    it('migrate deployed successfully',async ()=>{
        const address =this.tasksContract.address
        assert.notEqual(address,null);
        assert.notEqual(address,undefined);
        assert.notEqual(address,0x0);
        assert.notEqual(address,"");
    })
    it ('get Taks List',async()=>{
        const TasksCounter =await this.tasksContract.TaskCounter()
        const TaskCounter =await this.tasksContract.tasks(TasksCounter)
        assert.equal(task.id.toNumber(),TasksCounter);
        assert.equal(task.title, "mi primer tarea de ejemplo");
        assert.equal(task.description,"tengo pendientes");
        assert.equal(task.done, false);
        assert.equal(TasksCounter,1);
        
    })
    it ("task created succesfully",async() =>{
        const result = await this.tasksContract.createTask(
            "some task",
            "descrión 2"
            );
            const taskEvent = result.logs[0].args;
            const tasksCounter= await this.tasksContract.tasksCounter();
            assert.equal(tasksCounter,2);
            assert.equal(taskEvent.id.toNumber(),2);
            assert.equal(taskEvent.title,"some task");
            assert.equal(taskEvent.description,"descrión 2");
            assert.equal(taskEvent.done,false);
    });

    it('task toggle done',async ()=>{
        const result = await this.tasksContract.toggleDone(1);
        const taskEvent=result.logs[0].args;
        const task = await this.tasksContract.tasks(1);

        assert.equal(task.done,true);
        assert.equal(taskEvent.done,true);
        assert.equal(taskEvent.id,1);
        
    })
});








