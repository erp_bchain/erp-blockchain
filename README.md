# ERP+Blockchain



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/erp_bchain/erp-blockchain.git
git branch -M main
git push -uf origin main
```

## Usage
Para utilizarlo se requiere lo siguiente:
Instalación de librerias con pip 
asgiref==3.5.0
certifi==2019.6.16
chardet==3.0.4
Django==3.1.14
django-jazzmin==2.4.9
djangorestframework==3.11.2
idna==2.8
python manage.py makemigration
python manage.py runserver

para cargar truflle se requiere instalar lo siguiente:
npm i truffle -g
npm i ganache
npm i lite-server

Ejecutar  

python manage.py migrate
python manage.py runserver
npm run dev


