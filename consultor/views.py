from django.shortcuts import render,redirect
from procesos.models import RegistrodeProyecto
from .forms import *
from .models import *
# Create your views here.
def cargar_locaciones(request):
    zona_id = request.GET.get('zona')
    locacion = Locacion.objects.filter(zona_id=zona_id).order_by('nombre')

    locacion_id = request.GET.get('locacion')
    anexo = Anexo.objects.filter(locacion_id=locacion_id).order_by('nombre')
    context = {
        'locacion': locacion,
        'anexo': anexo
    }
    return render(request, 'others/locacion_dropdown_list_options.html', context)


def Registro_de_Consultores_Expertos_de_Proyectos(request):
    registro_proyecto = RegistrodeProyecto.objects.all()
    context = {'registro_proyecto': registro_proyecto}
    return render(request, 'consultor/proyecto-wise-consultor-registration.html', context)

def registro_consultor(request):
    formulario_info_academica = AcademicInfoForm(request.POST or None)
    formulario_info_personal = PersonalInfoForm(request.POST or None, request.FILES or None)
    formulario_info_direccion_consultor = DireccionConsultorInfoForm(request.POST or None)
    formulario_inf_defensa = DefensaInfoForm(request.POST or None)
    formulario_de_detalles_de_contacto_de_emergencia = DatosdeContactodeEmergenciaForm(request.POST or None)
    formulario_de_informacion_academica_anterior = AcademicadePreviaInfoForm(request.POST or None)
    formulario_de_certificado_academico_anterior = CertificadoAcademicoPrevioForm(request.POST or None, request.FILES)

    if request.method == 'POST':
        if formulario_info_academica.is_valid() and formulario_info_personal.is_valid() and formulario_info_direccion_consultor.is_valid() and formulario_inf_defensa.is_valid() and formulario_de_detalles_de_contacto_de_emergencia.is_valid() and formulario_de_informacion_academica_anterior.is_valid() and formulario_de_certificado_academico_anterior.is_valid():
            s1 = formulario_info_personal.save()
            s2 = formulario_info_direccion_consultor.save()
            s3 = formulario_inf_defensa.save()
            s4 = formulario_de_detalles_de_contacto_de_emergencia.save()
            s5 = formulario_de_informacion_academica_anterior.save()
            s6 = formulario_de_certificado_academico_anterior.save()
            academic_info = formulario_info_academica.save(commit=False)
            academic_info.personal_info = s1
            academic_info.direccion_info = s2
            academic_info.defensa_info = s3
            academic_info.contacto_emergencia_info = s4
            academic_info.informacion_academica_previa_info = s5 
            academic_info.certificado_academico_previo_info = s6 
            academic_info.save()
            return redirect('consultor-list')

    context = {
        
        'formulario_info_academica': formulario_info_academica,
        'formulario_info_personal': formulario_info_personal,
        'formulario_info_direccion_consultor': formulario_info_direccion_consultor,
        'formulario_inf_defensa': formulario_inf_defensa,
        'formulario_de_detalles_de_contacto_de_emergencia': formulario_de_detalles_de_contacto_de_emergencia,
        'formulario_de_informacion_academica_anterior': formulario_de_informacion_academica_anterior,
        'formulario_de_certificado_academico_anterior': formulario_de_certificado_academico_anterior
    }
    return render(request, 'consultor/consultor-registration.html', context)

def consultor_list(request):
    consultor = AcademicInfo.objects.filter(is_delete=False).order_by('-id')
    context = {'consultor': consultor}
    return render(request, 'consultor/consultor-list.html', context)

def consultor_profile(request, reg_no):
    consultor = AcademicInfo.objects.get(numero_registro=reg_no)
    context = {
        'consultor': consultor
    }
    return render(request, 'consultor/consultor-profile.html', context)

def consultor_edit(request, reg_no):
    consultor = AcademicInfo.objects.get(numero_registro=reg_no)
    formulario_info_academica = AcademicInfoForm(instance=consultor)
    formulario_info_personal = PersonalInfoForm(instance=consultor.personal_info)
    formulario_info_direccion_consultor = DireccionConsultorInfoForm(instance=consultor.direccion_info)
    formulario_info_defensa = DefensaInfoForm(instance=consultor.defensa_info)
    formulario_de_detalles_de_contacto_de_emergencia = DatosdeContactodeEmergenciaForm(instance=consultor.contacto_emergencia_info)
    formulario_de_informacion_academica_anterior = AcademicadePreviaInfoForm(instance=consultor.informacion_academica_previa_info)
    formulario_de_certificado_academico_anterior = CertificadoAcademicoPrevioForm(instance=consultor.certificado_academico_previo)

    if request.method == 'POST':
        formulario_info_academica = AcademicInfoForm(request.POST, instance=consultor)
        formulario_info_personal = PersonalInfoForm(request.POST, request.FILES, instance=consultor.personal_info)
        formulario_info_direccion_consultor = DireccionConsultorInfoForm(request.POST, instance=consultor.direccion_info)
        formulario_info_defensa = DefensaInfoForm(request.POST, instance=consultor.defensa_info)
        formulario_de_detalles_de_contacto_de_emergencia = DatosdeContactodeEmergenciaForm(request.POST, instance=consultor.contacto_emergencia_info)
        formulario_de_informacion_academica_anterior = AcademicadePreviaInfoForm(request.POST, instance=consultor.informacion_academica_previa_info)
        formulario_de_certificado_academico_anterior = CertificadoAcademicoPrevioForm(request.POST, request.FILES, instance=consultor.certificado_academico_previo)
        if formulario_info_academica.is_valid() and formulario_info_personal.is_valid() and formulario_info_direccion_consultor.is_valid() and formulario_info_defensa.is_valid() and formulario_de_detalles_de_contacto_de_emergencia.is_valid() and formulario_de_informacion_academica_anterior.is_valid() and formulario_de_certificado_academico_anterior.is_valid():
            s1 = formulario_info_personal.save()
            s2 = formulario_info_direccion_consultor.save()
            s3 = formulario_info_defensa.save()
            s4 = formulario_de_detalles_de_contacto_de_emergencia.save()
            s5 = formulario_de_informacion_academica_anterior.save()
            s6 = formulario_de_certificado_academico_anterior.save()
            academic_info.personal_info = s1
            academic_info.direccion_info = s2
            academic_info.defensa_info = s3
            academic_info.contacto_emergencia_info = s4
            academic_info.informacion_academica_previa_info = s5 
            academic_info.certificado_academico_previo_info = s6 
            academic_info.save()
            return redirect('consultor-list')

    context = {
        'formulario_info_academica': formulario_info_academica,
        'formulario_info_personal': formulario_info_personal,
        'formulario_info_direccion_consultor': formulario_info_direccion_consultor,
        'formulario_inf_defensa': formulario_inf_defensa,
        'formulario_de_detalles_de_contacto_de_emergencia': formulario_de_detalles_de_contacto_de_emergencia,
        'formulario_de_informacion_academica_anterior': formulario_de_informacion_academica_anterior,
        'formulario_de_certificado_academico_anterior': formulario_de_certificado_academico_anterior
    }
    return render(request, 'consultor/consultor-edit.html', context)

def consultor_delete(request, reg_no):
    consultor = AcademicInfo.objects.get(numero_registro=reg_no)
    consultor.is_delete = True
    consultor.save()
    return redirect('consultor-list')

def consultor_search(request):
    forms = BusquedaConsultoresForm()
    cls_nombre = request.GET.get('informacion_proyecto', None)
    reg_no = request.GET.get('numero_registro', None)
    if cls_nombre:
        consultor = AcademicInfo.objects.filter(proyecto_info=cls_nombre)
        if reg_no:
            consultor = consultor.filter(numero_registro=reg_no)
        context = {
            'forms': forms,
            'consultor': consultor
        }
        return render(request, 'consultor/consultor-search.html', context)
    else:
        consultor = AcademicInfo.objects.filter(numero_registro=reg_no)
        context = {
            'forms': forms,
            'consultor': consultor
        }
        return render(request, 'consultor/consultor-search.html', context)
    context = {
            'forms': forms,
            'consultor': consultor
        }
    return render(request, 'consultor/consultor-search.html', context)


def consultor_inscrito(request):
    forms = ConsultorInscritoForm()
    cls = request.GET.get('nombre_proyecto', None)
    consultor = AcademicInfo.objects.filter(proyecto_info=cls, estado='not enroll')
    context = {
        'forms': forms,
        'consultor': consultor
    }
    return render(request, 'consultor/enrolled.html', context)

def consultor_inscripcion(request, reg):
    consultor = AcademicInfo.objects.get(numero_registro=reg)
    forms = Inscripcion_de_ConsultorForm()
    if request.method == 'POST':
        forms = Inscripcion_de_ConsultorForm(request.POST)
        if forms.is_valid():
            roll = forms.cleaned_data['roll_no']
            nombre_proyecto = forms.cleaned_data['nombre_proyecto']
            ConsultorInscrito.objects.create(nombre_proyecto=nombre_proyecto, consultor=consultor, roll=roll)
            consultor.estado  = 'enrolled'
            consultor.save()
            return redirect('enrolled-consultor-list')
    context = {
        'consultor': consultor,
        'forms': forms
    }
    return render(request, 'consultor/consultor-enrolled.html', context)

def lista_consultor_inscrito(request):
    consultor = ConsultorInscrito.objects.all()
    forms = BuscarConsultorInscritoForm()  
    nombre_proyecto = request.GET.get('reg_proyecto', None)
    roll = request.GET.get('roll_no', None)
    if nombre_proyecto:
        consultor = ConsultorInscrito.objects.filter(nombre_proyecto=nombre_proyecto)
        context = {
            'forms': forms,
            'consultor': consultor
        }
        return render(request, 'consultor/enrolled-consultor-list.html', context)
    context = {
        'forms': forms,
        'consultor': consultor
    }
    return render(request, 'consultor/enrolled-consultor-list.html', context)