from django.urls import path
from . import views

urlpatterns = [
    path('proyecto-wise-consultor-registration', views.Registro_de_Consultores_Expertos_de_Proyectos, name='proyecto-wise-consultor-registration'),
    path('consultor-registration', views.registro_consultor, name='consultor-registration'),
    path('consultor-list', views.consultor_list, name='consultor-list'),

    path('profile/<reg_no>', views.consultor_profile, name='consultor-profile'),
    path('edit/<reg_no>', views.consultor_edit, name='consultor-edit'),
    path('delete/<reg_no>', views.consultor_delete, name='consultor-delete'),
    path('consultor-search/', views.consultor_search, name='consultor-search'),
    path('enrolled/', views.consultor_inscrito, name='enrolled-consultor'),
    path('enrolled-consultor/<reg>', views.consultor_inscripcion, name='enrolled'),
    path('enrolled-consultor-list/', views.lista_consultor_inscrito, name='enrolled-consultor-list'),
]
