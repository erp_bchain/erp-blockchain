from django.db import models
import random
from procesos.models import ProyectoInfo,RegistrodeProyecto
from domicilio.models import Zona,Locacion,Anexo
# Create your models here.

class PersonalInfo(models.Model):
    nombre = models.CharField('Nombre',max_length=100)
    foto = models.ImageField('Foto',upload_to='consultor-photos/')
    eleccion_del_grupo_sanguineo = (
        ('a+', 'A+'),
        ('o+', 'O+'),
        ('b+', 'B+'),
        ('ab+', 'AB+'),
        ('a-', 'A-'),
        ('o-', 'O-'),
        ('b-', 'B-'),
        ('ab-', 'AB-')
    )
    grupo_sanguineo = models.CharField('elección del grupo sanguíneo',choices=eleccion_del_grupo_sanguineo, max_length=5)
    fecha_de_nacimiento = models.DateField()
    eleccion_de_genero = (
        ('masculino', 'Masculino'),
        ('femenino', 'Femenino'),
        ('otro', 'Otro')
    )
    genero = models.CharField('Género',choices=eleccion_de_genero, max_length=10)
    numero_telefono = models.CharField('Número de telefono',max_length=11)
    email = models.EmailField(blank=True, null=True)
    certificado_de_nacimiento_no = models.IntegerField('certificado de nacimiento')
    eleccion_religion = (
        ('Islam', 'Islam'),
        ('Hinduismo', 'Hinduismo'),
        ('budismo', 'budismo'),
        ('cristianismo', 'cristianismo'),
        ('Otros', 'Otros')
    )
    religion = models.CharField('elección de religión',choices=eleccion_religion, max_length=45)
    eleccion_nacionalidad = (
        ('México', 'México'),
        ('Alemania', 'Alemania'),
        ('Rusia', 'Rusia'),
        ('Usa', 'Usa'),
        ('Otros', 'Otros')
    )
    nacionalidad = models.CharField('Nacionalidad',choices=eleccion_nacionalidad, max_length=45)

    def __str__(self):
        return self.nombre

class DireccionConsultorInfo(models.Model):
    zona = models.ForeignKey(Zona, on_delete=models.CASCADE)
    locacion = models.ForeignKey(Locacion, on_delete=models.CASCADE)
    anexo = models.ForeignKey(Anexo, on_delete=models.CASCADE)
    cp = models.TextField('C.P.')

    def __str__(self):
        return self.cp

class DefensaInfo(models.Model):
    nombre_padre = models.CharField('Nombre del padre',max_length=100)
    numero_telefono_padre = models.CharField('Número telefono',max_length=11)
    eleccion_de_ocupacion_del_padre = (
        ('Servicios Públicos', 'Servicios Públicos'),
        ('Servicios Privados', 'Servicios Privados'),
        ('Trabajo Informal', 'Trabajo Informal'),
        ('Obrero', 'Obrero'),
        ('Profesionista', 'Profesionista'),
        ('N/A', 'N/A'),
    )
    ocupacion_padre = models.CharField('Ocupación',choices=eleccion_de_ocupacion_del_padre, max_length=45)
    ingresos_anuales_del_padre = models.IntegerField()
    nombre_madre = models.CharField('Nombre de la Madre',max_length=100)
    numero_telefono_madre = models.CharField('Número de telefono',max_length=11)
    eleccion_de_ocupacion_del_madre = (
        ('Servicios Públicos', 'Servicios Públicos'),
        ('Servicios Privados', 'Servicios Privados'),
        ('Trabajo Informal', 'Trabajo Informal'),
        ('Obrero', 'Obrero'),
        ('Profesionista', 'Profesionista'),
        ('N/A', 'N/A'),
    )
    ocupacion_madre = models.CharField('Ocupación de la Madre',choices=eleccion_de_ocupacion_del_madre, max_length=45)
    nombre_supervisor = models.CharField('Nombre del Supervisor',max_length=100)
    numero_telefono_supervisor = models.CharField('Numero de Supervisor',max_length=11)
    email_supervisor = models.EmailField(blank=True, null=True)
    seleccion_relacion = (
        ('Representnte', 'Representnte'),
        ('Jefe_Inmediato', 'Jefe_Inmediato'),
        ('Asesor', 'Asesor'),
        ('Encargado_proyecto', 'Encargado_proyecto'),
        ('Encargado_asunto', 'Encargado_asunto'),
    )
    relacion_con_consultor = models.CharField('Relación con empleado',choices=seleccion_relacion, max_length=45)

    def __str__(self):
        return self.nombre_supervisor

class DatosdeContactodeEmergencia(models.Model):
    nombre_del_defensa_de_emergencia = models.CharField('Nombre de contacto de emergencia',max_length=100)
    direccion = models.TextField()
    seleccion_relacion = (
        ('Padre', 'Padre'),
        ('Madre', 'Madre'),
        ('Hermano', 'Hermano'),
        ('Tia', 'Tio'),
        ('Conyugue', 'Conyugue'),
    )
    relacion_con_consultor = models.CharField('Relación con empleado',choices=seleccion_relacion, max_length=45)
    numero_telefono = models.CharField('Número de telefono',max_length=11)
    email = models.EmailField(blank=True, null=True)

    def __str__(self):
        return self.nombre_del_defensa_de_emergencia

class AcademicadePreviaInfo(models.Model):
    nombre_instituto = models.CharField('Nombre del Instituto',max_length=100)
    nombre_evaluacion = models.CharField('Nombre de Evaluación',max_length=100)
    grupo = models.CharField('Grupo',max_length=45)
    gpa = models.CharField('GPA',max_length=10)
    tabla_roles = models.IntegerField('Tabla de roles')
    anio_actual = models.IntegerField('Año presente')

    def __str__(self):
        return self.nombre_instituto

class CertificadoAcademicoPrevio(models.Model):
    certificado_nacimiento = models.FileField('Acta de nacimiento',upload_to='documents/', blank=True)
    carta_de_libercion = models.FileField('carta de liberación',upload_to='documents/', blank=True)
    titulo_profesional = models.FileField('Titulo',upload_to='documents/', blank=True)
    kardex = models.FileField('Kardex',upload_to='documents/', blank=True)
    curriculum_vitae = models.FileField('CV',upload_to='documents/', blank=True)
    otros_certificados = models.FileField('Otras certificaciones',upload_to='documents/', blank=True)

class AcademicInfo(models.Model):
    proyecto_info = models.ForeignKey(ProyectoInfo, on_delete=models.CASCADE)
    numero_registro = models.IntegerField('Numero de Registro',unique=True, default=random.randint(000000, 999999))
    seleccion_estado = (
        ('alistado', 'alistado'),
        ('normal', 'normal'),
        ('irregular', 'irregular'),
        ('aprobado', 'aprobado'),
    )
    estado = models.CharField('Estado',choices=seleccion_estado, default='not enroll', max_length=15)
    personal_info = models.ForeignKey(PersonalInfo, on_delete=models.CASCADE, null=True)
    direccion_info = models.ForeignKey(DireccionConsultorInfo, on_delete=models.CASCADE, null=True)
    defensa_info = models.ForeignKey(DefensaInfo, on_delete=models.CASCADE, null=True)
    contacto_emergencia_info = models.ForeignKey(DatosdeContactodeEmergencia, on_delete=models.CASCADE, null=True)
    informacion_academica_previa_info = models.ForeignKey(AcademicadePreviaInfo, on_delete=models.CASCADE, null=True)
    certificado_academico_previo = models.ForeignKey(CertificadoAcademicoPrevio, on_delete=models.CASCADE, null=True)
    date = models.DateField(auto_now_add=True)
    is_delete = models.BooleanField('Elimminado',default=False)

    def __str__(self):
        return str(self.numero_registro)

class ConsultorInscrito(models.Model):
    nombre_proyecto = models.ForeignKey(RegistrodeProyecto, on_delete=models.CASCADE)
    consultor = models.OneToOneField(AcademicInfo, on_delete=models.CASCADE)
    roll = models.IntegerField('Rol')
    date = models.DateField(auto_now_add=True)

    class Meta:
        unique_together = ['nombre_proyecto', 'roll']
    
    def __str__(self):
        return str(self.roll)


