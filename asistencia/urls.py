from django.urls import path
from .views import asistencia_consultor,SetAsistencia

urlpatterns = [
    path('consultor/', asistencia_consultor, name='consultor-attendance'),
    path('set-attendance/<std_class>/<std_roll>', SetAsistencia.as_view(), name='set-attendance')
]