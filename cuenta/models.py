from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nombre = models.CharField('Nombre',max_length=45)
    foto = models.ImageField('Foto',upload_to='admin/')
    seleccion_de_genero = (
        ('masculino', 'Masculino'),
        ('femenino', 'Femenino')
    )
    genero = models.CharField(choices=seleccion_de_genero, max_length=15)
    seleccion_de_empleado = (
        ('admin', 'Admin'),
        ('gerente', 'Gerente'),
        ('registro', 'Registro'),
        ('empleado', 'Empleado'),
        ('consultor', 'Consultor'),
    )
    tipo_empleado = models.CharField(choices=seleccion_de_empleado, max_length=15)

    def __str__(self):
        return self.nombre

